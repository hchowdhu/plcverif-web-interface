import { useContext } from "react";
import Carousel from "../steps/Carousel";
import ExpandedViewCodeWriteContext from "../../store/expandedView-code-context";
import ExpandedViewCodeWrite from "../steps/code/ExpandedViewCodeWrite";
import ExpandedViewCaseWrite from "../steps/case/ExpandedViewCaseWrite";
import ExpandedViewCaseWriteContext from "../../store/expandedView-case-context";


function PageBody() {

    var expandedViewCodeWriteCtx = useContext(ExpandedViewCodeWriteContext);
    var expandedViewCaseWriteCtx = useContext(ExpandedViewCaseWriteContext);

    return (
        <>  
            <div style = {{height: "80vh"}}>
                <Carousel />
            </div>

            <div>
                {expandedViewCodeWriteCtx.isExpanded ? 
                    <ExpandedViewCodeWrite />
                : null}
            </div>

            <div>
                {expandedViewCaseWriteCtx.isExpanded ? 
                    <ExpandedViewCaseWrite />
                : null}
            </div>
        </>
    );
}

export default PageBody;