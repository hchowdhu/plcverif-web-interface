import { useContext } from "react";
import ColorThemeContext from "../../store/colorTheme-context";


function Header() {

    const colorThemeCtx = useContext(ColorThemeContext);


    function changePageTheme() {
        colorThemeCtx.setThemeNumber();
    }

    return (
        <>
            <nav className="navbar" style = {{background: colorThemeCtx.header}}>
                <ul className="nav justify-content-end">
                    <li className={"nav-item"} style={{margin: 'auto'}}>
                        <a className="nav-link active" aria-current="page" href = "/">
                            <img src = "plcverif_icon.png" width = "40rem"/>
                        </a>
                    </li>
                    <li className={"nav-item"} style={{margin: 'auto'}}>
                        <a className="nav-link" href="https://gitlab.com/plcverif-oss" target = "_blank">GitLab</a>
                    </li>
                    <li className={"nav-item"} style={{margin: 'auto'}}>
                        <a className="nav-link" href="https://plcverif-oss.gitlab.io/plcverif-docs/" target = "_blank">Docs</a>
                    </li>
                    <li className={"nav-item"} style={{margin: 'auto'}}>
                        <a className="nav-link" href = "https://www.home.cern/" target = "_blank">CERN</a>
                    </li>
                </ul>
            </nav>
            <p className="nav-link active" aria-current="page" 
                style = {{position: "absolute", top: "1.05rem", right: "1rem", textAlign: "right", color: "gray"}}>
                CERN PLCVerif | Web Interface
                <button type="button" className="btn" style = {{marginLeft: "1rem", color: colorThemeCtx.contentText2, border: "2px solid white", background: colorThemeCtx.expandedViewButtons}} onClick = {changePageTheme}>Change Theme</button>
            </p>
            {/* Note that the background of the button below is set to that of the expandedViewButtons, because it has the right color, but this button is not an expanded button */}
        </>
    )
}

export default Header;