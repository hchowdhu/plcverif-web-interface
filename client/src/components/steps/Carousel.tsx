import Code from "./code/Code";
import classes from "./Carouse.module.css"
import { useContext, useState } from "react";
import ExpandedViewCodeWriteContext from "../../store/expandedView-code-context";
import Case from "./case/Case";
import ExpandedViewCaseWriteContext from "../../store/expandedView-case-context";
import Report from "./report/Report";
import ColorThemeContext from "../../store/colorTheme-context";

function Carousel() {
    
    // Used the context here to check if the expanded view for writing code is shown
    // This is because the right and left buttons and the bottom buttons were not disappearing
    //  eventhough the expanded textarea should have a z-index higher than it. So, the buttons 
    // are made to disappear if the view is expanded for writing code.
    const expandedViewCodeWriteCtx = useContext(ExpandedViewCodeWriteContext);
    const expandedViewCaseWriteCtx = useContext(ExpandedViewCaseWriteContext);
    const colorThemeCtx = useContext(ColorThemeContext);

    const [currentCarouselTab, setCourselTab] = useState(0); // 0 is the first one

    return (
        <div id="carouselExampleIndicators" className={"carousel slide " + classes.dimensionPositionCarousel} data-bs-interval="false">
            { (expandedViewCodeWriteCtx.isExpanded ||  expandedViewCaseWriteCtx.isExpanded) ? null :
                <>
                    { currentCarouselTab != 0 ?
                        <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev"
                                style = {{width: "5%", background: colorThemeCtx.carouselSideButtons, border: "5px solid white", height: "80vh", borderRadius : "0.5rem"}}
                                onClick = {function(){setCourselTab(currentCarouselTab - 1)}}>
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="visually-hidden">Previous</span>
                        </button>
                    : null}
                    { currentCarouselTab != 2 ?
                    <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next"
                            style = {{width: "5%", background: colorThemeCtx.carouselSideButtons, border: "5px solid white", height: "80vh", borderRadius : "0.5rem"}}
                            onClick = {function(){setCourselTab(currentCarouselTab + 1)}}>
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Next</span>
                    </button>
                    : null}
                </>
            }
            <div className="carousel-inner">
                <div className="carousel-item active">
                    <Code />
                </div>
                <div className="carousel-item">
                    <Case />
                </div>
                <div className="carousel-item">
                    <Report />
                </div>
            </div>
        </div>
    );
}

export default Carousel;