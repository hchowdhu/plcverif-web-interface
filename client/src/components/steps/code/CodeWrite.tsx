import { useContext, useEffect, useRef } from "react";
import ExpandedViewCodeWriteContext from "../../../store/expandedView-code-context";
import VerificationContext from "../../../store/verification-context";
import ColorThemeContext from "../../../store/colorTheme-context";


function CodeWrite() {
    
    const expandedViewCodeWriteCtx = useContext(ExpandedViewCodeWriteContext);
    const verificationCtx = useContext(VerificationContext);
    const colorThemeCtx = useContext(ColorThemeContext);

    const writtenCodeRef = useRef(null);

    function updateExpandedViewCodeWrite() {
        expandedViewCodeWriteCtx.setExpandedView(true);
    }

    useEffect(function(){
        // When the tab changes, update the STL code. So, when the user click's the arrow button, it knows which tab the code is coming from.
        // We also have to update the code upon it changing, which is done below, in the onChange.
        verificationCtx.setSTLCode(writtenCodeRef.current == null ? "" : writtenCodeRef.current["value"]);
    });

    function updateWriteCodeCtxAndVerificationCtx() {
        expandedViewCodeWriteCtx.setWrittenCode(writtenCodeRef.current == null ? "" : writtenCodeRef.current["value"]);
        verificationCtx.setSTLCode(writtenCodeRef.current == null ? "" : writtenCodeRef.current["value"]);
    }

    return (
        <>
            <div className="card-body text-center" style = {{flexGrow: "1", padding: "0", margin: "0", display: "flex", flexDirection: "column"}}>
                <p className="card-text" style = {{fontSize: "1.1vw", color: colorThemeCtx.contentText, marginTop: "1rem"}}>Write the code in the area below. Then, click the arrow on the right.</p>
                <div style = {{display: "flex", justifyContent: "center", alignItems: "center", padding: "0px"}}>
                    <button type="button" className="btn btn-outline" onClick = {updateExpandedViewCodeWrite} style = {{fontSize: "1vw", background: colorThemeCtx.expandedViewButtons, color: colorThemeCtx.contentText2, border: "2px solid white"}}>Expand View</button>
                </div>
                <textarea onChange = {updateWriteCodeCtxAndVerificationCtx} defaultValue = {expandedViewCodeWriteCtx.writtenCode} ref = {writtenCodeRef} className="form-control" id="exampleFormControlTextarea1" rows={5} 
                    style = {{whiteSpace: "nowrap", resize: "none", fontSize: "1.5vh", margin: "2%", flexGrow: "1", padding: "10px", border: "none", width: "96%"}}>
                </textarea>
            </div>
        </>
    );
}

export default CodeWrite;