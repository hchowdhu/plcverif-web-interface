
import { useContext, useRef } from "react";
import classes from "./ExpandedViewCodeWrite.module.css"
import ExpandedViewCodeWriteContext from "../../../store/expandedView-code-context";

function ExpandedViewCodeWrite() {

    const expandedViewCodeWriteCtx = useContext(ExpandedViewCodeWriteContext);
    const writtenCodeExpandedRef = useRef(null);

    function exitExpandedView() {
        expandedViewCodeWriteCtx.setExpandedView(false);
        expandedViewCodeWriteCtx.setWrittenCode(writtenCodeExpandedRef.current == null ? "" : writtenCodeExpandedRef.current["value"]);
    }
    
    return (
        <>
            <textarea defaultValue = {expandedViewCodeWriteCtx.writtenCode} ref = {writtenCodeExpandedRef} className={"form-control " + classes.dimensionsAndPositionTextArea} id="exampleFormControlTextarea3"></textarea>
            <button type="button" className={"btn btn-success " + classes.buttonPositioning} onClick = {exitExpandedView}>Go Back</button>
        </>
    );
}

export default ExpandedViewCodeWrite;