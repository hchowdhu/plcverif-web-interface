import classes from "../Steps.module.css"
import CodeWrite from "./CodeWrite";
import { useState, useContext } from "react";
import CodeFile from "./CodeFile";
import ExpandedViewCodeWriteContext from "../../../store/expandedView-code-context";
import ColorThemeContext from "../../../store/colorTheme-context";

function Code() {   

    const [tabState, updateTabState] = useState(true); // true means "Type code here", false means "Upload a file"
    
    const expandedViewCodeWriteCtx = useContext(ExpandedViewCodeWriteContext);
    const colorThemeCtx = useContext(ColorThemeContext);

    function toggleTab() {
        updateTabState(!tabState);
        
    }

    return (
        <div className={"card " + classes.dimensionPosition} style = {{background: colorThemeCtx.carouselBackground, height: "80vh"}}>
            { expandedViewCodeWriteCtx.isExpanded ? null :
                <>
                    <div className="card-header" style = {{background: colorThemeCtx.carouselTabHeader}}>
                        {/* Do note that the style for the background of the button group is carouselSideButtons, only because the color suits it. This button group is not the carousel side buttons though. */}
                        <div className="btn-group" role="group" aria-label="Basic radio toggle button group" style = {{background: colorThemeCtx.carouselSideButtons}}>
                            <input type="radio" className="btn-check" name="btnradio" id="btnradio1" autoComplete="off" checked = {tabState} onClick = {tabState == false ? toggleTab : function(){}}/>
                            <label className="btn btn-outline-danger" htmlFor="btnradio1" style = {{fontSize: "0.8vw", color: colorThemeCtx.contentText2}}>Type code here</label>

                            <input type="radio" className="btn-check" name="btnradio" id="btnradio2" autoComplete="off" checked = {!tabState} onClick = {tabState == true ? toggleTab : function(){}}/>
                            <label className="btn btn-outline-danger" htmlFor="btnradio2" style = {{fontSize: "0.8vw", color: colorThemeCtx.contentText2}}>Upload a file</label>
                        </div>
                        <h5 className="card-title text-center" style = {{position: "absolute", top: "1rem", right: "1rem", fontSize: "1.1vw", color: "black"}}><b>Step 1. STL Code</b></h5>
                    </div>

                    <div style = {{height: "80vh", display: "flex", flexDirection: "column"}}>
                        {tabState ? <CodeWrite /> : <CodeFile />}
                    </div>
                </>
            }    
        </div>
    );
}

export default Code;