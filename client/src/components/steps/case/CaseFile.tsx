import { useContext, useEffect, useRef, useState } from "react";
import VerificationContext from "../../../store/verification-context";
import ColorThemeContext from "../../../store/colorTheme-context";

function CaseFile() {

    const previewRef = useRef(null);
    const [preview, setPreview] = useState("");

    const verificationCtx = useContext(VerificationContext);
    const colorThemeCtx = useContext(ColorThemeContext);

    function updatePreview(file: File | undefined) {
        const reader = new FileReader();
        if(file != undefined){
            reader.readAsText(file);
            reader.addEventListener('load', (e) => {
                const data = e.target == null ? "" : e.target.result;
                setPreview((data == null ? "" : data).toString());
            });
        }

        verificationCtx.setVC3Case(previewRef.current == null ? "" : previewRef.current["value"]);
    }

    useEffect(function(){
        // When the tab changes, update the case. So, when the user click's the arrow button, it knows which tab the case is coming from.
        // We also have to update the case upon it changing, which is done above, in the onChange function called updatePreview.
        verificationCtx.setVC3Case(previewRef.current == null ? "" : previewRef.current["value"]);
    });

    return (
        <div className="card-body" style = {{color: colorThemeCtx.contentText, flexGrow: "1", padding: "0", margin: "0", display: "flex", flexDirection: "column"}}>
            <p className="card-text text-center" style = {{fontSize: "1.1vw", marginTop: "1rem"}}>Upload an VC3 file below. Then, click the arrow on the right.</p>
            
            <div className="mb-3" style = {{width : "30%", position: "relative", left : "35%"}}>
                <input style = {{fontSize: "0.7vw"}} accept = ".vc3" className="form-control" type="file" id="formFile" onChange = 
                    {
                        function (e) {
                            updatePreview(
                                e.target == null ? 
                                    undefined : e.target["files"] == null ? 
                                        undefined : e.target["files"][0]);
                        }
                    }
                />
            </div>

            <textarea ref = {previewRef} className="form-control" id="exampleFormControlTextarea6" rows={5} readOnly value = {preview} style = {{resize: "none", fontSize: "1.5vh", margin: "2%", flexGrow: "1", padding: "10px", border: "none", width: "96%"}}></textarea>
        </div>
    );
}

export default CaseFile;