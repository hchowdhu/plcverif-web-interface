
import { useContext, useRef } from "react";
import classes from "./ExpandedViewCaseWrite.module.css"
import ExpandedViewCaseWriteContext from "../../../store/expandedView-case-context";

function ExpandedViewCaseWrite() {

    const expandedViewCaseWriteCtx = useContext(ExpandedViewCaseWriteContext);
    const writtenCaseExpandedRef = useRef(null);

    function exitExpandedView() {
        expandedViewCaseWriteCtx.setExpandedView(false);
        expandedViewCaseWriteCtx.setWrittenCase(writtenCaseExpandedRef.current == null ? "" : writtenCaseExpandedRef.current["value"]);
    }
    
    return (
        <>
            <textarea defaultValue = {expandedViewCaseWriteCtx.writtenCase} ref = {writtenCaseExpandedRef} className={"form-control " + classes.dimensionsAndPositionTextArea} id="exampleFormControlTextarea5"></textarea>
            <button type="button" className={"btn btn-success " + classes.buttonPositioning} onClick = {exitExpandedView}>Go Back</button>
        </>
    );
}

export default ExpandedViewCaseWrite;