import classes from "../Steps.module.css"
import { useContext, useState } from "react";
import CaseBuild from "./CaseBuild";
import CaseWrite from "./CaseWrite";
import CaseFile from "./CaseFile";
import ExpandedViewCaseWriteContext from "../../../store/expandedView-case-context";
import ColorThemeContext from "../../../store/colorTheme-context";

function Case() {   

    const [tabState, updateTabState] = useState(0); // 0 is build, 1 is write, 2 is file

    const expandedViewCaseWriteCtx = useContext(ExpandedViewCaseWriteContext);
    const colorThemeCtx = useContext(ColorThemeContext);

    function toggleTab(tabNum: number) {
        updateTabState(tabNum);
    }

    // The condition for expandedViewCaseWriteCtx.isExpanded == false is there because otherwise the tab buttons would show in the expanded view. I put this here and not in Carousel.tsx 
    // because there, it would reset the Case.tsx rendering after you click "Go back" which would make the tab shift to "Build with a guide." So, when you clicked go back, you would go back
    // to the first tab and not "Type code here."
    return ( 
        <div className={"card " + classes.dimensionPosition} style = {{background: colorThemeCtx.carouselBackground, height: "80vh"}}>
            { expandedViewCaseWriteCtx.isExpanded ? null :
                <>
                    <div className="card-header" style = {{background: colorThemeCtx.carouselTabHeader}}>
                        {/* Do note that the style for the background of the button group is carouselSideButtons, only because the color suits it. This button group is not the carousel side buttons though. */}
                        <div className="btn-group" role="group" aria-label="Basic radio toggle button group" style = {{background: colorThemeCtx.carouselSideButtons}}>
                            <input type="radio" className="btn-check" name="btnradio1" id="btnradio4" autoComplete="off" checked = {tabState == 0 ? true : false} onClick = {function(){toggleTab(0)}}/>
                            <label className="btn btn-outline-danger" htmlFor="btnradio4"  style = {{fontSize: "0.8vw", color: colorThemeCtx.contentText2}}>Build with a guide</label>
                            
                            <input type="radio" className="btn-check" name="btnradio1" id="btnradio5" autoComplete="off" checked = {tabState == 1 ? true : false} onClick = {function(){toggleTab(1)}}/>
                            <label className="btn btn-outline-danger" htmlFor="btnradio5"  style = {{fontSize: "0.8vw", color: colorThemeCtx.contentText2}}>Type case here</label>

                            <input type="radio" className="btn-check" name="btnradio1" id="btnradio6" autoComplete="off" checked = {tabState == 2 ? true : false} onClick = {function(){toggleTab(2)}}/>
                            <label className="btn btn-outline-danger" htmlFor="btnradio6"  style = {{fontSize: "0.8vw", color: colorThemeCtx.contentText2}}>Upload a file</label>
                        </div>
                        <h5 className="card-title text-center" style = {{position: "absolute", top: "1rem", right: "1rem", fontSize: "1.1vw", color: "black"}}><b>Step 2. Verification Case</b></h5>
                    </div>

                    <div style = {{height: "80vh", display: "flex", flexDirection: "column"}}>
                        {tabState == 0 ? <CaseBuild /> :
                            tabState == 1 ? <CaseWrite /> : <CaseFile />}
                    </div>
                </>
            }
        </div>
    );
}

export default Case;