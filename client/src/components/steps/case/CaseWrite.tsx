import { useContext, useEffect, useRef } from "react";
import ExpandedViewCaseWriteContext from "../../../store/expandedView-case-context";
import VerificationContext from "../../../store/verification-context";
import ColorThemeContext from "../../../store/colorTheme-context";


function CaseWrite() {
    
    const expandedViewCaseWriteCtx = useContext(ExpandedViewCaseWriteContext);
    const verificationCtx = useContext(VerificationContext);
    const colorThemeCtx = useContext(ColorThemeContext);
    
    const writtenCaseRef = useRef(null);

    function updateExpandedViewCaseWrite() {
        expandedViewCaseWriteCtx.setExpandedView(true);
    }

    useEffect(function(){
        // When the tab changes, update the case. So, when the user click's the arrow button, it knows which tab the case is coming from.
        // We also have to update the case upon it changing, which is done below, in the onChange.
        verificationCtx.setVC3Case(writtenCaseRef.current == null ? "" : writtenCaseRef.current["value"]);
    });

    function updateWriteCaseCtxAndVerificationCtx() {
        expandedViewCaseWriteCtx.setWrittenCase(writtenCaseRef.current == null ? "" : writtenCaseRef.current["value"]);
        verificationCtx.setVC3Case(writtenCaseRef.current == null ? "" : writtenCaseRef.current["value"]);
    }

    return (
        <>
            <div className="card-body text-center" style = {{flexGrow: "1", padding: "0", margin: "0", display: "flex", flexDirection: "column"}}>
                <p className="card-text" style = {{fontSize: "1.1vw", color: colorThemeCtx.contentText, marginTop: "1rem"}}>Write the case in the area below. Then, click the arrow on the right.</p>
                <div style = {{display: "flex", justifyContent: "center", alignItems: "center", padding: "0px"}}>
                    <button type="button" className="btn btn-outline" onClick = {updateExpandedViewCaseWrite} style = {{fontSize: "1vw", background: colorThemeCtx.expandedViewButtons, color: colorThemeCtx.contentText2, border: "2px solid white"}}>Expand View</button>
                </div>
                <textarea onChange = {updateWriteCaseCtxAndVerificationCtx} defaultValue = {expandedViewCaseWriteCtx.writtenCase} ref = {writtenCaseRef} className="form-control" id="exampleFormControlTextarea4" rows={15} 
                    style = {{whiteSpace: "nowrap", resize: "none", fontSize: "1.5vh", margin: "2%", flexGrow: "1", padding: "10px", border: "none", width: "96%"}}>
                </textarea>
            </div>
        </>
    );
}

export default CaseWrite;