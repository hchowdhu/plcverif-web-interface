
import { useState, useRef, useContext } from "react";
import classes from "./CaseBuild.module.css"
import CaseBuildingContext from "../../../store/caseBuilding-context";
import VerificationContext from "../../../store/verification-context";

function CaseBuild() {

    const caseBuildingCtx = useContext(CaseBuildingContext);
    const verificationCtx = useContext(VerificationContext);

    const [selectedBackend, setSelectedBackend] = useState(caseBuildingCtx.verificationBackend.backend == "" ? "NuSMV" : caseBuildingCtx.verificationBackend.backend);
    const [selectedRequirement, setSelectedRequirement] = useState(caseBuildingCtx.requirement.type == "" ? "Assertion" : caseBuildingCtx.requirement.type);

    const [subStep, setSubStep] = useState(0); // initally, it is at "1. Metadata"
    const [numberOfPatternReqTextAreas, setNumberOfTextAreas] = useState(computePatternNumberOfTextAreas(caseBuildingCtx.requirement.pattern == "" ? "pattern-implication" : caseBuildingCtx.requirement.pattern));
    
    function updateSelectedBackend(event: any) {
        setSelectedBackend(event.target.value);
    }

    function updateSelectedRequirement(event: any) {
        setSelectedRequirement(event.target.value);
    }

    function updateSubStep(step: number) {
        setSubStep(step);
    }

    function updatePatternNumberOfTextAreas(event: any) {
        setNumberOfTextAreas(computePatternNumberOfTextAreas(event.target.value));
    }

    function computePatternNumberOfTextAreas(value: string) {
        switch(value) {
            case "pattern-implication":
                return 2;
                break;
            case "pattern-invariant":
                return 1;
                break;
            case "pattern-forbidden":
                return 1;
                break;
            case "pattern-statechange-duringcycle":
                return 2;
                break;
            case "pattern-statechange-betweencycles":
                return 3;
                break;
            case "pattern-reachability":
                return 1;
                break;
            case "pattern-repeatability":
                return 1;
                break;
            case "pattern-leadsto":
                return 2;
                break;
            default:
                return 1;
        }
    }

    const mdIDRef = useRef(null);
    const mdNameRef = useRef(null);
    const mdDescriptionRef = useRef(null);
    const vbBackendRef = useRef(null);
    const vbBackendNuSMVAlgorithmRef = useRef(null);
    const vbBackendCBMCESBMCModVarRef = useRef(null);
    const reqReqtypeRef = useRef(null);
    const reqReqtypeAssCheckRef = useRef(null);
    const reqReqtypePatPatRef = useRef(null);
    const reqReqtypePat1Ref = useRef(null);
    const reqReqtypePat2Ref = useRef(null);
    const reqReqtypePat3Ref = useRef(null);
    const entryBlockRef = useRef(null);
    
    function saveBuildingProgress() {
        const mdIDRefVAL = mdIDRef.current == null ? "" : mdIDRef.current["value"];
        const mdNameRefVAL = mdNameRef.current == null ? "" : mdNameRef.current["value"];
        const mdDescriptionRefVAL = mdDescriptionRef.current == null ? "" : mdDescriptionRef.current["value"];
        const vbBackendRefVAL = vbBackendRef.current == null ? "" : vbBackendRef.current["value"];
        const vbBackendNuSMVAlgorithmRefVAL = vbBackendNuSMVAlgorithmRef.current == null ? "" : vbBackendNuSMVAlgorithmRef.current["value"];
        const vbBackendCBMCESBMCModVarRefVAL = vbBackendCBMCESBMCModVarRef.current == null ? "" : vbBackendCBMCESBMCModVarRef.current["value"];
        const reqReqtypeRefVAL = reqReqtypeRef.current == null ? "" : reqReqtypeRef.current["value"];
        const reqReqtypeAssCheckRefVAL = reqReqtypeAssCheckRef.current == null ? "" : reqReqtypeAssCheckRef.current["value"];
        const reqReqtypePatPatRefVAL = reqReqtypePatPatRef.current == null ? "" : reqReqtypePatPatRef.current["value"];
        const reqReqtypePat1RefVAL = reqReqtypePat1Ref.current == null ? "" : reqReqtypePat1Ref.current["value"];
        const reqReqtypePat2RefVAL = reqReqtypePat2Ref.current == null ? "" : reqReqtypePat2Ref.current["value"];
        const reqReqtypePat3RefVAL = reqReqtypePat3Ref.current == null ? "" : reqReqtypePat3Ref.current["value"];
        const entryBlockRefVAL = entryBlockRef.current == null ? "" : entryBlockRef.current["value"];

        caseBuildingCtx.setMetaData({
            "id": mdIDRefVAL,
            "name": mdNameRefVAL,
            "description": mdDescriptionRefVAL
        });

        if(vbBackendRefVAL.toString() == "NuSMV"){
            caseBuildingCtx.setVerificationBackend({
                "backend": vbBackendRefVAL,
                "algorithm": vbBackendNuSMVAlgorithmRefVAL
            });
        } else if(vbBackendRefVAL.toString() == "CBMC" || vbBackendRefVAL.toString() == "ESBMC") {
            caseBuildingCtx.setVerificationBackend({
                "backend": vbBackendRefVAL,
                "modelVariant": vbBackendCBMCESBMCModVarRefVAL,
            });
        }
        if(reqReqtypeRefVAL.toString() == "Assertion"){
            caseBuildingCtx.setRequirement({
                "type": reqReqtypeRefVAL,
                "check":  reqReqtypeAssCheckRefVAL,
            });
        } else if(reqReqtypeRefVAL.toString() == "Division by zero"){
            caseBuildingCtx.setRequirement({
                "type": reqReqtypeRefVAL,
            });
        } else if(reqReqtypeRefVAL.toString() == "Pattern"){
            caseBuildingCtx.setRequirement({
                "type": reqReqtypeRefVAL,
                "pattern": reqReqtypePatPatRefVAL,
                "one": reqReqtypePat1RefVAL,
                "two": reqReqtypePat2RefVAL,
                "three": reqReqtypePat3RefVAL
            });
        }

        caseBuildingCtx.setEntryBlock(
            entryBlockRefVAL
        );
    }

    function returnCorrespondingModelVariantValues(modVar: string) {
        var valueToReturn = ""
        switch(modVar) {
            case "CFA declaration":
                valueToReturn = "CFD"
                break;
            case "CFA instance":
                valueToReturn = "CFI"
                break;
            case "Structured CFA declaration":
                valueToReturn = "CFD_STRUCTURED";
                break;
        }
        return valueToReturn;
    }

    function createCase() {
        const mdIDRefVAL = mdIDRef.current == null ? "" : mdIDRef.current["value"];
        const mdNameRefVAL = mdNameRef.current == null ? "" : mdNameRef.current["value"];
        const mdDescriptionRefVAL = mdDescriptionRef.current == null ? "" : mdDescriptionRef.current["value"];
        const vbBackendRefVAL = vbBackendRef.current == null ? "" : vbBackendRef.current["value"];
        const vbBackendNuSMVAlgorithmRefVAL = vbBackendNuSMVAlgorithmRef.current == null ? "" : vbBackendNuSMVAlgorithmRef.current["value"];
        const vbBackendCBMCESBMCModVarRefVAL = vbBackendCBMCESBMCModVarRef.current == null ? "" : vbBackendCBMCESBMCModVarRef.current["value"];
        const reqReqtypeRefVAL = reqReqtypeRef.current == null ? "" : reqReqtypeRef.current["value"];
        const reqReqtypeAssCheckRefVAL = reqReqtypeAssCheckRef.current == null ? "" : reqReqtypeAssCheckRef.current["value"];
        const reqReqtypePatPatRefVAL = reqReqtypePatPatRef.current == null ? "" : reqReqtypePatPatRef.current["value"];
        const reqReqtypePat1RefVAL = reqReqtypePat1Ref.current == null ? "" : reqReqtypePat1Ref.current["value"];
        const reqReqtypePat2RefVAL = reqReqtypePat2Ref.current == null ? "" : reqReqtypePat2Ref.current["value"];
        const reqReqtypePat3RefVAL = reqReqtypePat3Ref.current == null ? "" : reqReqtypePat3Ref.current["value"];
        const entryBlockRefVAL = entryBlockRef.current == null ? "" : entryBlockRef.current["value"];

        var verification_case = "";

        var backendSettingToAdd = ((vbBackendRefVAL.toString() == "NuSMV") ? 
            (vbBackendNuSMVAlgorithmRefVAL.toString() == "Default" ? "" : "-job.backend.algorithm = " + vbBackendNuSMVAlgorithmRefVAL+ "\r\n")
            :
            (vbBackendRefVAL.toString() == "CBMC" || vbBackendRefVAL.toString() == "ESBMC") ?
                (vbBackendCBMCESBMCModVarRefVAL.toString() == "Default" ? "" :  "-job.backend.model_variant = " + returnCorrespondingModelVariantValues(vbBackendCBMCESBMCModVarRefVAL) + "\r\n")
                : ""
        );

        if(reqReqtypeRefVAL.toString() == "Pattern"){
            verification_case = 
                "-description = \"" + mdDescriptionRefVAL + "\"\r\n"
                + "-id = " + mdIDRefVAL + "\r\n"
                + "-job = verif\r\n"
                + "-job.backend = " + vbBackendRefVAL.toLowerCase() + "\r\n"
                + backendSettingToAdd
                + "-job.req = pattern\r\n"
                + "-job.req.pattern_id = " + reqReqtypePatPatRefVAL + "\r\n"
                + "-job.req.pattern_params.1 = \"" + reqReqtypePat1RefVAL + "\"\r\n"
                + "-job.req.pattern_params.2 = \"" + reqReqtypePat2RefVAL + "\"\r\n"
                + "-job.req.pattern_params.3 = \"" + reqReqtypePat3RefVAL + "\"\r\n"
                + "-lf = step7\r\n"
                + "-lf.entry = " + entryBlockRefVAL + "\r\n"
                + "-name = \"" + mdNameRefVAL + "\"\r\n"
        } else if(reqReqtypeRefVAL.toString() == "Division by zero") {
            verification_case = 
                "-description = \"" + mdDescriptionRefVAL + "\"\r\n"
                + "-id = " + mdIDRefVAL + "\r\n"
                + "-job = verif\r\n"
                + "-job.backend = " + vbBackendRefVAL.toLowerCase() + "\r\n"
                + backendSettingToAdd
                + "-job.req = divbyzero\r\n"
                + "-lf = step7\r\n"
                + "-lf.entry = " + entryBlockRefVAL + "\r\n"
                + "-name = \"" + mdNameRefVAL + "\"\r\n"
        } else if(reqReqtypeRefVAL.toString() == "Assertion") {
            verification_case = 
                "-description = \"" + mdDescriptionRefVAL + "\"\r\n"
                + "-id = " + mdIDRefVAL + "\r\n"
                + "-job = verif\r\n"
                + "-job.backend = " + vbBackendRefVAL.toLowerCase() + "\r\n"
                + backendSettingToAdd
                + "-job.req = assertion\r\n"
                + "-job.req.assertion_to_check.0 = " + reqReqtypeAssCheckRefVAL + "\r\n"
                + "-lf = step7\r\n"
                + "-lf.entry = " + entryBlockRefVAL + "\r\n"
                + "-name = \"" + mdNameRefVAL + "\"\r\n"
        }
        
        verificationCtx.setVC3Case(verification_case);
    }
    

    return (
        <>
            <div className={"row row-cols-1 row-cols-md-3 g-3 " + classes.dimensionPosition} style = {{fontSize: "1vw"}} onChange = {saveBuildingProgress}>
                <div className="col" style = {{height: "100%"}}>
                    <div className= {"card h-100 text-bg-" + (subStep == 0 ? "success" : "secondary")}>
                        <div className="card-body">
                            <div className="card-title" style = {{fontSize: "1.2vw"}}><b>1. Metadata</b></div>
                            
                            <div className="mb-3">
                                <label htmlFor="exampleFormControlInput1" className="form-label">ID:</label>
                                <input defaultValue = {caseBuildingCtx.metadata.id} type="email" className="form-control" id="exampleFormControlInput1" placeholder="case" style = {{fontSize: "1vw"}} disabled = {subStep == 0 ? false : true} ref = {mdIDRef}/>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="exampleFormControlInput2" className="form-label">Name:</label>
                                <input defaultValue = {caseBuildingCtx.metadata.name} type="email" className="form-control" id="exampleFormControlInput2" placeholder="example" style = {{fontSize: "1vw"}} disabled = {subStep == 0 ? false : true} ref = {mdNameRef}/>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="exampleFormControlTextarea7" className="form-label">Description:</label>
                                <textarea defaultValue = {caseBuildingCtx.metadata.description} className="form-control" id="exampleFormControlTextarea7" rows={3} style = {{resize: "none", fontSize: "0.8vw"}} disabled = {subStep == 0 ? false : true} ref = {mdDescriptionRef}></textarea>
                            </div>
                            
                            <div className="container text-center">
                                <div className="col">
                                    <div className="d-grid gap-2">
                                        <button className="btn btn-info" type="button" onClick = {function(){updateSubStep(subStep + 1)}} style = {{fontSize: "0.8vw"}} disabled = {subStep == 0 ? false : true}>Next</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <div className= {"card h-100 text-bg-" + (subStep == 1 ? "success" : "secondary")}>
                        <div className="card-body">
                            <h5 className="card-title" style = {{fontSize: "1.2vw"}}><b>2. Verification Backend</b></h5>

                            <label className="form-label" style = {{paddingTop: "1rem"}}>Entry Block:</label>
                            <input defaultValue = {caseBuildingCtx.entryBlock} type="text" className="form-control" id="staticEmail" style = {{fontSize: "1vw"}} ref = {entryBlockRef} disabled = {subStep == 1 ? false : true}/>

                            <label className="form-label" style = {{paddingTop: "1rem"}}>Backend:</label>
                            <select defaultValue = {caseBuildingCtx.verificationBackend.backend} className="form-select" aria-label="Default select example" onChange = {updateSelectedBackend}  style = {{fontSize: "1vw"}} disabled = {subStep == 1 ? false : true} ref = {vbBackendRef}>
                                <option value="NuSMV" selected>NuSMV</option>
                                <option value="CBMC">CBMC</option>
                                <option value="ESBMC">ESBMC</option>
                            </select>

                            {
                                    selectedBackend == "NuSMV" ? 
                                        <div>
                                            <label className="form-label" style = {{paddingTop: "1rem"}}>Algorithm:</label>
                                            <select defaultValue = {caseBuildingCtx.verificationBackend.algorithm} className="form-select form-select-sm" aria-label="Small select example"  style = {{fontSize: "1vw"}} disabled = {subStep == 1 ? false : true} ref = {vbBackendNuSMVAlgorithmRef}>
                                                <option value="Default" selected>Default</option>
                                                <option value="Classic">Classic</option>
                                                <option value="Ic3">Ic3</option>
                                            </select>
                                        </div>
                                        : (selectedBackend == "CBMC" || selectedBackend == "ESBMC") ?
                                            <div>
                                                <label className="form-label" style = {{paddingTop: "1rem"}}>Model Variant:</label>
                                                <select defaultValue = {caseBuildingCtx.verificationBackend.modelVariant} className="form-select form-select-sm" aria-label="Small select example"  style = {{fontSize: "1vw"}} disabled = {subStep == 1 ? false : true} ref = {vbBackendCBMCESBMCModVarRef}>
                                                    <option value="Default" selected>Default</option>
                                                    <option value="CFA declaration">CFA declaration</option>
                                                    <option value="CFA instance">CFA instance</option>
                                                    <option value="Structured CFA declaration">Structured CFA declaration</option>
                                                </select>
                                            </div>
                                            : null
                            }

                            <br></br>
                            <div className="container text-center">
                                <div className="row">
                                    <div className="col">
                                        <div className="d-grid gap-2">
                                            <button className="btn btn-info" type="button" onClick = {function(){updateSubStep(subStep - 1)}} style = {{fontSize: "0.8vw"}} disabled = {subStep == 1 ? false : true}>Back</button>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="d-grid gap-2">
                                            <button className="btn btn-info" type="button" onClick = {function(){updateSubStep(subStep + 1)}} style = {{fontSize: "0.8vw"}} disabled = {subStep == 1 ? false : true}>Next</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <div className= {"card h-100 text-bg-" + (subStep == 2 ? "success" : "secondary")}>
                        <div className="card-body">
                            <h5 className="card-title" style = {{fontSize: "1.2vw"}}><b>3. Requirement</b></h5>
                            
                            <label className="form-label">Requirement Type:</label>
                            <select defaultValue = {caseBuildingCtx.requirement.type} className="form-select" aria-label="Default select example" onChange = {updateSelectedRequirement} style = {{fontSize: "1vw"}} disabled = {subStep == 2 ? false : true} ref = {reqReqtypeRef}>
                                <option value="Assertion" selected>Assertion</option>
                                <option value="Division by zero">Division by zero</option>
                                <option value="Pattern">Pattern</option>
                            </select>

                            {
                                selectedRequirement == "Assertion" ?
                                    <div>
                                        <label htmlFor="exampleFormControlTextarea8" className="form-label" style = {{paddingTop: "1rem"}}>Assertions to check:</label>
                                        <textarea defaultValue = {caseBuildingCtx.requirement.check} className="form-control" id="exampleFormControlTextarea8" rows={3} style = {{whiteSpace: "nowrap", resize: "none", fontSize: "0.7vw"}} disabled = {subStep == 2 ? false : true} ref = {reqReqtypeAssCheckRef}></textarea>
                                    </div>
                                    : selectedRequirement == "Division by zero" ?
                                        <div>
                                            <label className="form-label" style = {{paddingTop: "1rem"}}><small>No further input required!</small></label>
                                        </div>
                                        : selectedRequirement == "Pattern" ?
                                            <div>
                                                <label className="form-label" style = {{paddingTop: "1rem"}}>Pattern:</label>
                                                <select onChange = {updatePatternNumberOfTextAreas} defaultValue = {caseBuildingCtx.requirement.pattern} className="form-select" aria-label="Default select example" style = {{fontSize: "0.9vw"}} disabled = {subStep == 2 ? false : true} ref = {reqReqtypePatPatRef}>
                                                    <option value="pattern-implication" selected>If (1) is true at the end of the PLC cycle, then (2) should always be true at the end of the same cycle.</option>
                                                    <option value="pattern-invariant">(1) is always true at the end of the PLC cycle.</option>
                                                    <option value="pattern-forbidden">(1) is impossible at the end of the PLC cycle.)</option>
                                                    <option value="pattern-statechange-duringcycle">If (1) is true at the beginning of the PLC cycle, then (2) is always true at the end of the same cycle.</option>
                                                    <option value="pattern-statechange-betweencycles">If (1) is true at the end of the cycle N and (2) is true at the end of the cycle N+1, then (3) is always true at the end of cycle N+1</option>
                                                    <option value="pattern-reachability">It is possible to have (1) at the end of the cycle.</option>
                                                    <option value="pattern-repeatability">Any time it is possible to have eventually (1) at the end of a cycle.</option>
                                                    <option value="pattern-leadsto">If (1) is true at the end of a cycle, (2) was true at the end of an earlier cycle.</option>
                                                </select>

                                                <div className="container text-center">
                                                    <div className="row">
                                                        <div className="col" style = {{padding: "1px"}}>
                                                            <label htmlFor="exampleFormControlTextarea9" className="form-label" style = {{paddingTop: "1rem"}}>1:</label>
                                                            <textarea defaultValue = {caseBuildingCtx.requirement.one} className="form-control" id="exampleFormControlTextarea9" rows={5} style = {{whiteSpace: "nowrap", resize: "none", fontSize: "0.7vw", width: "100%"}} disabled = {subStep == 2 ? false : true} ref = {reqReqtypePat1Ref}></textarea>
                                                        </div>
                                                        {numberOfPatternReqTextAreas >= 2 ? 
                                                            <div className="col" style = {{padding: "1px"}}>
                                                                <label htmlFor="exampleFormControlTextarea10" className="form-label" style = {{paddingTop: "1rem"}}>2:</label>
                                                                <textarea defaultValue = {caseBuildingCtx.requirement.two} className="form-control" id="exampleFormControlTextarea10" rows={5} style = {{whiteSpace: "nowrap", resize: "none", fontSize: "0.7vw", width: "100%"}} disabled = {subStep == 2 ? false : true} ref = {reqReqtypePat2Ref}></textarea>
                                                            </div>
                                                            : null
                                                        }
                                                        {numberOfPatternReqTextAreas == 3 ? 
                                                            <div className="col" style = {{padding: "1px"}}>
                                                                <label htmlFor="exampleFormControlTextarea11" className="form-label" style = {{paddingTop: "1rem"}}>3:</label>
                                                                <textarea defaultValue = {caseBuildingCtx.requirement.three} className="form-control" id="exampleFormControlTextarea11" rows={5} style = {{whiteSpace: "nowrap", resize: "none", fontSize: "0.7vw", width: "100%"}} disabled = {subStep == 2 ? false : true} ref = {reqReqtypePat3Ref}></textarea>
                                                            </div>
                                                            : null
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                            : null
                            }

                            <br></br>
                            <div className="container text-center">
                                <div className="row">
                                    <div className="col">
                                        <div className="d-grid gap-2">
                                            <button className="btn btn-info" type="button" onClick = {function(){updateSubStep(subStep - 1)}} style = {{fontSize: "0.8vw"}} disabled = {subStep == 2 ? false : true}>Back</button>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="d-grid gap-2">
                                            <button className="btn btn-info" type="button" onClick = {createCase} style = {{fontSize: "0.8vw"}} disabled = {subStep == 2 ? false : true} data-bs-toggle="modal" data-bs-target="#exampleModal">Create Case</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="modal fade" id="exampleModal" tabIndex={-1} aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header" style = {{background: "lightgreen"}}>
                            <h1 className="modal-title fs-5" id="exampleModalLabel"><b>Case created!</b></h1>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            You can now get the report by clicking the button on the right.
                            <br></br><br></br>
                            Here is a preview of your case file:
                            <textarea style = {{width: "100%"}} rows = {10} readOnly value = {verificationCtx.vc3Case}></textarea>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default CaseBuild;