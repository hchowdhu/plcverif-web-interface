import { useContext, useState } from "react";
import classes from "../Steps.module.css"
import VerificationContext from "../../../store/verification-context";
import ColorThemeContext from "../../../store/colorTheme-context";

function Report() {   

    const [loading, setLoading] = useState(false);
    const [serverDown, setServerDown] = useState(false);

    const verificationCtx = useContext(VerificationContext);
    var colorThemeCtx = useContext(ColorThemeContext);

    const getReport = async () => {
        setLoading(true);

        var reportBody = document.getElementById("reportBody");
        if(reportBody != null) {
            reportBody.innerHTML = "";
        }

        const data = {
            stlCode: verificationCtx.stlCode,
            vc3Case: verificationCtx.vc3Case
        };
        
        // http://localhost:8082/
        // https://backend-plcverifweb.app.cern.ch/
        try {
            await fetch("https://backend-plcverifweb.app.cern.ch/", {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: data.stlCode
            });
        } catch (err) {
            console.log("ErrorSCL: " + err);
        }

        try {
            const response = await fetch("https://backend-plcverifweb.app.cern.ch/", {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: data.vc3Case
            });

            if(response.ok) {
                setServerDown(false);
                setLoading(false);

                var responseData = await response.text();
                
                if(reportBody != null) {
                    responseData = responseData
                        .replace("href=\"javascript:toggleVisibility('expertDetails')\"", "href = \"#\" onclick=\"document.getElementById('expertDetails').className=(document.getElementById('expertDetails').className=='hidden')?'':'hidden';\"")
                        .replace("href=\"javascript:toggleVisibility('expertDetails')\"", "href = \"#\" onclick=\"document.getElementById('expertDetails').className=(document.getElementById('expertDetails').className=='hidden')?'':'hidden';\"")

                    reportBody.innerHTML = responseData;
                }
            }
        } catch (err) {
            console.log("ErrorVC3: " + err);

            setServerDown(true);
            setLoading(false);
        }
    };

    return (
        <div className={"card " + classes.dimensionPosition} style = {{background: colorThemeCtx.carouselBackground, height: "80vh", display: "flex"}}>
            <div className="card-header" style = {{background: colorThemeCtx.carouselTabHeader}}>
                <div className="btn-group" role="group" aria-label="Basic radio toggle button group">
                    {/* Note that the background of the button below is set to that of the expandedViewButtons, because it has the right color, but this button is not an expanded button */}
                    <button type="button" className="btn" style = {{fontSize: "0.8vw", color: colorThemeCtx.contentText2, border: "2px solid white", background: colorThemeCtx.expandedViewButtons}} onClick = {getReport}>Get Report</button>
                </div>
                <h5 className="card-title text-center" style = {{position: "absolute", top: "1rem", right: "1rem", fontSize: "1.1vw", color: "black"}}><b>Step 3. Receive the Report</b></h5>
            </div>
            
            {loading ? 
                <div className="card-body" style = {{margin: "0", position: "absolute", top: "50%", left: "50%", transform: "translate(-50%, -50%)", msTransform: "translate(-50%, -50%)"}}>
                    <button className="btn btn-primary" type="button" disabled style = {{overflow: "hidden", whiteSpace: "nowrap"}}>
                        <span className="spinner-border spinner-border-sm" aria-hidden="true" style = {{fontSize: "5vw"}}></span>
                        <span role="status" style = {{paddingLeft: "1vw"}}>Waiting for report...</span>
                    </button>
                </div>  
            : 
            <div id = "reportBody" style = {{background: "white", position: "relative", top: "0rem", overflow: "auto", flex: "1", padding: "10px"}}>{serverDown ? "Backend server is offline" : ""}</div>
            }
        </div>
    );
}

export default Report;