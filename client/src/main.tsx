import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import 'bootstrap/dist/css/bootstrap.css'
import { BrowserRouter } from 'react-router-dom'
import { VerificationContextProvider } from './store/verification-context.tsx'
import { ExpandedViewCodeWriteContextProvider } from './store/expandedView-code-context.tsx'
import { ExpandedViewCaseWriteContextProvider } from './store/expandedView-case-context.tsx'
import { CaseBuildingContextProvider } from './store/caseBuilding-context.tsx'
import { ColorThemeContextProvider } from './store/colorTheme-context.tsx'

ReactDOM.createRoot(document.getElementById('root')!).render(
	<React.StrictMode>
		<VerificationContextProvider>
			<ExpandedViewCodeWriteContextProvider>
				<ExpandedViewCaseWriteContextProvider>
					<CaseBuildingContextProvider>
						<ColorThemeContextProvider>
							<BrowserRouter>
								<App />
							</BrowserRouter>
						</ColorThemeContextProvider>
					</CaseBuildingContextProvider>
				</ExpandedViewCaseWriteContextProvider>
			</ExpandedViewCodeWriteContextProvider>
		</VerificationContextProvider>
  	</React.StrictMode>,
)
