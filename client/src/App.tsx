
import { useContext } from 'react';
import './App.css'
import Header from './components/common/Header';
import PageBody from './components/common/PageBody';
import ColorThemeContext from './store/colorTheme-context';

function App() {

	const colorThemeCtx = useContext(ColorThemeContext);

	var body = document.getElementsByTagName("body")[0];
	if(body != null) {
		body.setAttribute('style', 'font-family: JetBrains Mono; background: ' + colorThemeCtx.background);
	}

	return (
		<>
			<Header />

			<PageBody />
		</>
	);
}

export default App;
