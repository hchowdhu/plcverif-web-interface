import { ReactNode, createContext, useState } from "react";

const CaseBuildingContext = createContext({
    metadata: {
        id: "",
        name: "",
        description: ""
    },
    entryBlock: "",
    verificationBackend: {
        backend: "",
        algorithm: "",
        modelVariant: "",
    },
    requirement: {
        type: "",
        check: "",
        pattern: "",
        one: "",
        two: "",
        three: ""
    },
    setMetaData: (_metadata: any) => {},
    setEntryBlock: (_entryblock: string) => {},
    setVerificationBackend: (_verificationBackend: any) => {},
    setRequirement: (_requirement: any) => {}
});

interface Props {
    children: ReactNode;
}

export function CaseBuildingContextProvider({children} : Props){

    const [currentMetaData, setNewMetaData] = useState({
        id: "",
        name: "",
        description: ""
    });
    const [currentEntryBlock, setNewEntryBlock] = useState("");
    const [currentVerificationBackend, setNewVerificationBackend] = useState({
        backend: "",
        algorithm: "",
        modelVariant: "",
    });
    const [currentRequirement, setNewRequirement] = useState({
        type: "",
        check: "",
        pattern: "",
        one: "",
        two: "",
        three: ""
    });

    function setNewCurrentMetaData(newMetaData: any) {
        setNewMetaData(newMetaData);
    }

    function setNewCurrentEntryBlock(newEntryBlock: string) {
        setNewEntryBlock(newEntryBlock);
    }

    function setNewCurrentVerificationBackend(newVerificationBackend: any) {
        setNewVerificationBackend(newVerificationBackend);
    }

    function setNewCurrentRequirement(newRequirement: any) {
        setNewRequirement(newRequirement);
    }
    
    const context = {
        metadata: currentMetaData,
        entryBlock: currentEntryBlock,
        verificationBackend: currentVerificationBackend,
        requirement: currentRequirement,
        setMetaData: setNewCurrentMetaData,
        setEntryBlock: setNewCurrentEntryBlock,
        setVerificationBackend: setNewCurrentVerificationBackend,
        setRequirement: setNewCurrentRequirement
    };

    return (
        <CaseBuildingContext.Provider value = {context}>
            {children}
        </CaseBuildingContext.Provider>
    );
}

export default CaseBuildingContext;