import { ReactNode, createContext, useState } from "react";

const ExpandedViewCodeWriteContext = createContext({
    isExpanded: false,
    writtenCode: "",
    setWrittenCode: (_code: string) => {},
    setExpandedView: (_expand: boolean) => {},
});

interface Props {
    children: ReactNode;
}

export function ExpandedViewCodeWriteContextProvider({children} : Props){

    const [currentExpandedState, setNewExpandedState] = useState(false);
    const [currentWrittenCode, setNewWrittenCode] = useState("");

    function updateWrittenCode(code: string) {
        setNewWrittenCode(code);
    }

    function updateExpandedView(expand: boolean) {
        setNewExpandedState(expand);
    }

    const context = {
        isExpanded: currentExpandedState,
        writtenCode: currentWrittenCode,
        setWrittenCode: updateWrittenCode,
        setExpandedView: updateExpandedView,
    };

    return (
        <ExpandedViewCodeWriteContext.Provider value = {context}>
            {children}
        </ExpandedViewCodeWriteContext.Provider>
    );
}

export default ExpandedViewCodeWriteContext;