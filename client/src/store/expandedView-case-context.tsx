import { ReactNode, createContext, useState } from "react";

const ExpandedViewCaseWriteContext = createContext({
    isExpanded: false,
    writtenCase: "",
    setWrittenCase: (_case: string) => {},
    setExpandedView: (_expand: boolean) => {},
});

interface Props {
    children: ReactNode;
}

export function ExpandedViewCaseWriteContextProvider({children} : Props){

    const [currentExpandedState, setNewExpandedState] = useState(false);
    const [currentWrittenCase, setNewWrittenCase] = useState("");

    function updateWrittenCase(newCase: string) {
        setNewWrittenCase(newCase);
    }

    function updateExpandedView(expand: boolean) {
        setNewExpandedState(expand);
    }

    const context = {
        isExpanded: currentExpandedState,
        writtenCase: currentWrittenCase,
        setWrittenCase: updateWrittenCase,
        setExpandedView: updateExpandedView,
    };

    return (
        <ExpandedViewCaseWriteContext.Provider value = {context}>
            {children}
        </ExpandedViewCaseWriteContext.Provider>
    );
}

export default ExpandedViewCaseWriteContext;