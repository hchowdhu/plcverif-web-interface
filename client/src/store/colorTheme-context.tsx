import { ReactNode, createContext, useState } from "react";

const ColorThemeContext = createContext({
	setThemeNumber: () => {},

    background: "",
    header: "",
    carouselSideButtons: "",
    carouselBackground: "",
    carouselTabHeader: "",
    contentText: "",
    contentText2: "",
    expandedViewButtons: "",
    textareas: "",
    setBackground: (_color: string) => {},
    setHeader: (_color: string) => {},
    setCarouselSideButtons: (_color: string) => {},
    setCarouselBackground: (_color: string) => {},
    setCarouselTabHeader: (_color: string) => {},
    setContentText: (_color: string) => {},
    setContentText2: (_color: string) => {},
    setExpandedViewButtons: (_color: string) => {},
    setTextareas: (_color: string) => {}
});

interface Props {
    children: ReactNode;
}

export function ColorThemeContextProvider({children} : Props){

    var themes = [
        {
          "name": "Shades of Blue 1",
          "colors": ["#001F3F", "#003366", "#004C99", "#0066CC", "#0077FF", "#3399FF", "#66B2FF", "#99CCFF"]
        },
        {
          "name": "Shades of Green 1",
          "colors": ["#003300", "#00591E", "#008038", "#00A055", "#00C372", "#17E890", "#4DF0B1", "#85FFD2"]
        },
        {
          "name": "Shades of Red 1",
          "colors": ["#330000", "#661A1A", "#992E2E", "#CC4444", "#FF6666", "#FF8080", "#FF9999", "#FFB3B3"]
        },
        {
          "name": "Shades of Purple 1",
          "colors": ["#330033", "#661A66", "#993399", "#CC66CC", "#FF99FF", "#FFB3FF", "#FFCCFF", "#FFD6FF"]
        },
        {
          "name": "Shades of Orange 1",
          "colors": ["#662200", "#994C00", "#CC6600", "#FF8000", "#FF9933", "#FFB266", "#FFCC99", "#FFE5CC"]
        },
        {
          "name": "Shades of Pink 1",
          "colors": ["#660033", "#99004D", "#CC0066", "#FF007F", "#FF3399", "#FF66B2", "#FF99CC", "#FFCCE5"]
        },
        {
          "name": "Shades of Teal 1",
          "colors": ["#004C4C", "#007070", "#009494", "#00B8B8", "#00DBDB", "#4DE3E3", "#99ECEC", "#E5F7F7"]
        },
        {
          "name": "Shades of Brown 1",
          "colors": ["#331A00", "#663300", "#994C00", "#CC6600", "#FF8000", "#FF9933", "#FFB266", "#FFCC99"]
        },
        {
          "name": "Shades of Grey 1",
          "colors": ["#111111", "#333333", "#555555", "#777777", "#999999", "#BBBBBB", "#DDDDDD", "#F5F5F5"]
        },
        {
          "name": "Shades of Cyan 1",
          "colors": ["#003333", "#006666", "#009999", "#00CCCC", "#00FFFF", "#4DFFFF", "#99FFFF", "#E5FFFF"]
        },
        {
          "name": "Shades of Lavender 1",
          "colors": ["#330066", "#660099", "#9900CC", "#CC00FF", "#E61AFF", "#F345FF", "#FF6FFF", "#FF9AFF"]
        },
        {
          "name": "Shades of Maroon 1",
          "colors": ["#330000", "#660000", "#990000", "#CC0000", "#FF0000", "#FF3333", "#FF6666", "#FF9999"]
        },
        {
          "name": "Shades of Indigo 1",
          "colors": ["#00264D", "#004080", "#0059B3", "#0073E6", "#0099FF", "#33ADFF", "#66C2FF", "#99D6FF"]
        },
        {
          "name": "Shades of Beige 1",
          "colors": ["#8D8468", "#A0997E", "#B2AF93", "#C3C4A7", "#D4D9BC", "#E6E9D0", "#F7F9E5", "#FAFBEF"]
        },
        {
          "name": "Shades of Ruby 1",
          "colors": ["#7A003C", "#9B185A", "#BD2D79", "#DE4298", "#FF57B7", "#FF75C8", "#FF94D9", "#FFB3EA"]
        },
        {
          "name": "Shades of Mint 1",
          "colors": ["#26704A", "#33935F", "#3FA674", "#4BBA8A", "#57DD9F", "#6CEBB3", "#82F9C8", "#99F7DC"]
        },
        {
          "name": "Shades of Slate 1",
          "colors": ["#2D3A4B", "#465666", "#5E6F80", "#777F9B", "#919AB5", "#AAA4CF", "#C2BEEA", "#DBD9FF"]
        }
    ];

	var currentThemeNumber = 13; // initial theme
	const randomizeColorsInTheme = false;
    var indexes = [0, 1, 2, 3, 4, 5, 6, 7];
    function randomColorFromRandomTheme(themeIndex: number) {
        const randomIndex = indexes[Math.floor(Math.random() * indexes.length)];
        const randomColor = themes[themeIndex].colors[randomIndex];
        indexes.splice(randomIndex, 1);
        return randomColor;
    }

	const [currentBackground, setCurrentBackground] = useState(randomizeColorsInTheme ? randomColorFromRandomTheme(currentThemeNumber) : themes[currentThemeNumber].colors[0]); //
    const [currentHeader, setCurrentHeader] = useState(randomizeColorsInTheme ? randomColorFromRandomTheme(currentThemeNumber) : themes[currentThemeNumber].colors[7]); //
    const [currentCarouseSideButtons, setCurrentCarouseSideButtons] = useState(randomizeColorsInTheme ? randomColorFromRandomTheme(currentThemeNumber) : themes[currentThemeNumber].colors[2]); //
    const [currentCarouselBackground, setCurrentCarouselBackground] = useState(randomizeColorsInTheme ? randomColorFromRandomTheme(currentThemeNumber) : themes[currentThemeNumber].colors[3]); //
    const [currentCarouselTabHeader, setCurrentCarouselTabHeader] = useState(randomizeColorsInTheme ? randomColorFromRandomTheme(currentThemeNumber) : themes[currentThemeNumber].colors[4]); //
    const [currentContentText, setCurrentContentText] = useState(randomizeColorsInTheme ? randomColorFromRandomTheme(currentThemeNumber) : themes[currentThemeNumber].colors[7]); //
    const [currentContentText2, setCurrentContentText2] = useState(randomizeColorsInTheme ? randomColorFromRandomTheme(currentThemeNumber) : themes[currentThemeNumber].colors[7]); //
    const [currentExpandedViewButtons, setCurrentExpandedViewButtons] = useState(randomizeColorsInTheme ? randomColorFromRandomTheme(currentThemeNumber) : themes[currentThemeNumber].colors[1]); //
    const [currentTextAreas, setCurrentTextAreas] = useState(randomizeColorsInTheme ? randomColorFromRandomTheme(currentThemeNumber) : themes[currentThemeNumber].colors[7]); 

	function updateThemeNumber() {
		currentThemeNumber = Math.floor(Math.random() * themes.length);
		console.log(themes[currentThemeNumber].name + " at index: " + currentThemeNumber);

		setCurrentBackground(randomizeColorsInTheme ? randomColorFromRandomTheme(currentThemeNumber) : themes[currentThemeNumber].colors[0]);
		setCurrentHeader(randomizeColorsInTheme ? randomColorFromRandomTheme(currentThemeNumber) : themes[currentThemeNumber].colors[7]);
		setCurrentCarouseSideButtons(randomizeColorsInTheme ? randomColorFromRandomTheme(currentThemeNumber) : themes[currentThemeNumber].colors[2]);
		setCurrentCarouselBackground(randomizeColorsInTheme ? randomColorFromRandomTheme(currentThemeNumber) : themes[currentThemeNumber].colors[3]);
		setCurrentCarouselTabHeader(randomizeColorsInTheme ? randomColorFromRandomTheme(currentThemeNumber) : themes[currentThemeNumber].colors[4]);
		setCurrentContentText(randomizeColorsInTheme ? randomColorFromRandomTheme(currentThemeNumber) : themes[currentThemeNumber].colors[6]);
		setCurrentContentText2(randomizeColorsInTheme ? randomColorFromRandomTheme(currentThemeNumber) : themes[currentThemeNumber].colors[7]);
		setCurrentExpandedViewButtons(randomizeColorsInTheme ? randomColorFromRandomTheme(currentThemeNumber) : themes[currentThemeNumber].colors[1]);
		setCurrentTextAreas(randomizeColorsInTheme ? randomColorFromRandomTheme(currentThemeNumber) : themes[currentThemeNumber].colors[7]);
	}

    function updateBackground(color: string) {
        setCurrentBackground(color);
    }
    function updateHeader(color: string) {
        setCurrentHeader(color);
    }
    function updateCarouselSideButtons(color: string) {
        setCurrentCarouseSideButtons(color);
    }
    function updateCarouselBackground(color: string) {
        setCurrentCarouselBackground(color);
    }
    function updateCarouselTabHeader(color: string) {
        setCurrentCarouselTabHeader(color);
    }
    function updateContentText(color: string) {
        setCurrentContentText(color);
    }
    function updateContentText2(color: string) {
        setCurrentContentText2(color);
    }
    function updateExpandedViewButtons(color: string) {
        setCurrentExpandedViewButtons(color);
    }
    function updateTextAreas(color: string) {
        setCurrentTextAreas(color);
    }

    const context = {
		setThemeNumber: updateThemeNumber,

        background: currentBackground,
        header: currentHeader,
        carouselSideButtons: currentCarouseSideButtons,
        carouselBackground: currentCarouselBackground,
        carouselTabHeader: currentCarouselTabHeader,
        contentText: currentContentText,
        contentText2: currentContentText2,
        expandedViewButtons: currentExpandedViewButtons,
        textareas: currentTextAreas,
        setBackground: updateBackground,
        setHeader: updateHeader,
        setCarouselSideButtons: updateCarouselSideButtons,
        setCarouselBackground: updateCarouselBackground,
        setCarouselTabHeader: updateCarouselTabHeader,
        setContentText: updateContentText,
        setContentText2: updateContentText2,
        setExpandedViewButtons: updateExpandedViewButtons,
        setTextareas: updateTextAreas
    };

    return (
        <ColorThemeContext.Provider value = {context}>
            {children}
        </ColorThemeContext.Provider>
    );
}

export default ColorThemeContext;