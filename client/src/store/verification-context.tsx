import { ReactNode, createContext, useState } from "react";

const VerificationContext = createContext({
    stlCode: "",
    vc3Case: "",
    setVC3Case: (_vc3Case: any) => {},
    setSTLCode: (_code: any) => {}
});

interface Props {
    children: ReactNode;
}

export function VerificationContextProvider({children} : Props){

    const [currentSTLCode, setNewSTLCode] = useState("");
    const [currentVC3Case, setNewVC3Case] = useState("");

    function setNewSTLCodeFunction(code: any) {
        setNewSTLCode(code);
    }

    function setNewVC3CaseFunction(vc3Case: any) {
        setNewVC3Case(vc3Case);
    }

    const context = {
        stlCode: currentSTLCode,
        vc3Case: currentVC3Case,
        setVC3Case: setNewVC3CaseFunction,
        setSTLCode: setNewSTLCodeFunction
    };

    return (
        <VerificationContext.Provider value = {context}>
            {children}
        </VerificationContext.Provider>
    );
}

export default VerificationContext;